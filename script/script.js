let button = document.querySelector('.open-modal')
let modal = document.querySelector('.modal')
let closeBtn = document.querySelector('.close-btn')
let crossBtn = document.querySelector('.cross_btn')

closeBtn.addEventListener('click', function (event) {
    modal.classList.remove('open')
})

crossBtn.addEventListener('click', function (event) {
    modal.classList.remove('open')
})

button.addEventListener('click', function (event) {
    modal.classList.toggle('open')
})


// modal.addEventListener('click', function (event) {
//     if (this === event.target) {
//         this.classList.toggle('open')
//     }
// })